const bcrypt = require("bcrypt");

const User = require("../model/users");
const auth = require("../auth")

// User registration
module.exports.registerUser = (req, res) => {
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        userName: req.body.userName,
        password: bcrypt.hashSync(req.body.password, 10),
        passwordLength: req.body.password.length
    })

    return User.findOne({userName: req.body.userName}).then(result => {
        if(result !== null){
            console.log(result)
            res.status(400).json("Username is already in use.");
        }
        else{
            newUser.save().then((user, error) => {
                if(user){
                    console.log(user);
                    res.status(200).json("Successful registration");
                }
                else{
                    console.log("Error registering user: ", error);
                    res.status(500).json(error);
                }
            })         
        }
    })
    .catch(error => {
        console.log("Error caught: ", error)
        res.status(500).json(error);
    })
}

// Login
module.exports.loginUser = (req, res) => {
    return User.findOne({userName: req.body.userName}).then(result => {
        if(result == null){
            res.status(400).json("User not found.");
        }
        else{
            const userPassword = bcrypt.compareSync(req.body.password, result.password);

            if(userPassword){
                res.status(200).json({access: auth.createAccessToken(result)});
            }
            else{
                res.status(400).json("Incorrect password.");
            }
        }
    })
    .catch(err => {
        console.log("Error Logging in: ", err);
        res.status(500).json(err)
    });
};

// Get User Details
module.exports.getProfile = (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    return User.findById(userData.id).then(result => {
        let passwordLength = result.passwordLength;
        let emptyPassword = "*".repeat(passwordLength);
        
        result.password = emptyPassword;
        res.status(200).json(result);
    })
    .catch(err => {
        console.log("Error fetching profile: ", err);
        res.status(500).json(err)
    })
}


module.exports.getTaskBreakdown = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    return User.findById(userData.id).then(result => {
        if(result){
            // const breakdown = result.task[0].map(task => task.breakdown);
            const task = result.task;
            // console.log(task.breakdown._id)
            res.send (task);
        }
        else{
            res.status(400).json("No available task.")
        }

    })
    .catch(err => res.status(500).json(err))
}