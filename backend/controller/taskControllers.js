const Task = require("../model/tasks");
const auth = require("../auth");
const User = require("../model/users");

module.exports.createTask = async (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);
    
    const foundUser = await User.findById(userData.id);
    
    if(foundUser){
        let newTask = new Task({
            description: req.body.description,
            increment: req.body.increment,
            weeks: req.body.weeks
        });   
        
        return newTask.save().then(result => { 
            let currentValue = 0;
            let total = 0;
            let week = 1; 
            let breakdown = [];
           
            console.log('description: ', newTask.description)
            for(week=1; week<=newTask.weeks; week++){

                currentValue += newTask.increment;
                breakdown.push({
                    totalAmount: total + currentValue,
                    week: week,
                    expectedAmount: currentValue
                    
                })
                total += currentValue;
                
            }
            
            result.user.push({
                userId: userData.id, 
                userName: userData.userName, 
                userTasks: breakdown
            })
            result.save();
            
            // const findTask = Task.findById(result._id)

            foundUser.task.push({
                // taskId: result._id.toString(),
                description: result.description,
                increment: result.increment,
                totalAmount: total,
                breakdown: breakdown
            })
            foundUser.save();
                 
            console.log(result._id)
            res.status(200).json(result) 
        })
        .catch( error => {
            res.status(500).json(error);
        })
    }
    else{
        res.status(400).json("No User found. Task was not created.");
    }

}


module.exports.showAll = (req, res) => {
    const loggedUser = auth.decode(req.headers.authorization);

    return Task.find().then(result => {
        // Filter tasks based on the condition
        const filteredTasks = result.filter(item => {
            const userIds = item.user.map(user => user.userId);
            return userIds.includes(loggedUser.id);
        });

        if (filteredTasks.length > 0) {
            res.status(200).json(filteredTasks);
        } else {
            res.status(400).json("No task created for this user.");
        }
    })
    .catch(err => {
        res.status(500).json(err);
    });
}



module.exports.getTaskById = (request, response) => {

    const taskId = request.params.taskId;

    return Task.findById(taskId).then(result => response.json(result))
    .catch(err => {
        response.status(500).json(err)
    })
}







