const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const taskController = require("../controller/taskControllers");

router.post('/createTask', taskController.createTask);

router.get('/list', taskController.showAll);

router.get('/:taskId', taskController.getTaskById);




module.exports = router;