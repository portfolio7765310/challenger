const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

const userController = require("../controller/userControllers");
const auth = require("../auth");

router.post("/register", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/profile", auth.verify, userController.getProfile);

router.get("/breakdown", userController.getTaskBreakdown);

module.exports = router;