const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required."]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required."]
    },
    userName: {
        type: String,
        required: [true, "Username is required."]
    },
    password: {
        type: String,
        required: true
    },
    passwordLength: {
        type: Number,
        default: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    task: [{
        // taskId: {
        //     type: String,
        //     required: [true, 'taskId is missing']
        // },
        description: {
            type: String,
            required: [true, "Description is required."]
        },
        increment: {
            type: Number,
            required: [true, "Increment value is required"]
        },
        totalAmount: {
            type: Number,
            required: [true, "Total amount is required for this user."]
        },
        date: {
            type: Date,
            default: new Date()
        },
        breakdown: [{
            // description: {
            //     type: String,
            //     required: [true, "Breakdown task description is missing"]
            // },
            week: {
                type: Number,
                required: [true, "Week must be added to user's breakdown."]
            },
            expectedAmount: {
                type: Number,
                required: [true, "Expected Amount is not detailed in this user's breakdown."]
            },
            // sum: {
            //     type: Number,
            //     required: [true, "Sum is missing."]
            // }
        }]
    }]
})

module.exports = mongoose.model("User", userSchema);