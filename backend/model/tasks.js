const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
    description: {
        type: String,
        required: [true, "Description is required."]
    },
    increment: {
        type: Number,
        required: [true, "Increment is required."]
    },
    weeks: {
        type: Number,
        required: [true, "Week is required."]
    },
    user: [{
        userId: {
            type: String,
            required: [true, "User ID is missing for this TASK."]
        },
        userName: {
            type: String,
            required: [true, "User name is missing for this TASK."]
        },
        userTasks: [{
            totalAmount: {
                type: Number,
                required: [true, 'totalAmount for userTasks is missing.']
            },
            week: {
                type: Number,
                required: [true, 'userTasks week is missing']
            },
            expectedAmount: {
                type: Number,
                required: [true, 'userTasks Amount is missing']
            }
        }],
        createdOn: {
            type: Date,
            default: new Date()
        },
    }],
    date: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Task", taskSchema);