const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");

const userRoute = require("./route/userRoutes");
const taskRoute = require("./route/taskRoutes");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/user", userRoute);
app.use("/task", taskRoute);


mongoose.connect("mongodb+srv://admin:admin@cluster0.1veqg2g.mongodb.net/?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to DB"));

app.listen(process.env.PORT || 3000, () => {console.log(`API is now online on port ${process.env.PORT || 3000}`)})