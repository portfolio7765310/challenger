const jwt = require("jsonwebtoken");
const secretKey = "ip0nProject";

// Token creator
module.exports.createAccessToken = user => {
    // payload
    const data = {
        id: user._id,
        userName: user.userName,
        password: user.password
    }

    return jwt.sign(data, secretKey, {/*expiresIn: "60s" */});
}

// Token verifier
// to verify a token from POSTMAN request
module.exports.verify = (request, response, next) => {
    let token = request.headers.authorization;

    if(typeof token !== "undefined"){
        console.log("Token: ", token);
        // removes the first 7 characters from token ("Bearer ")
        token = token.slice(7, token.length);

        return jwt.verify(token, secretKey, (error, data) => {
            if(error){
                response.status(500).json({auth: "Failed"});
            }
            else{
                next();
            }
        })
    }
    else if(!token){
        response.status(400).json("Token is missing.")
    }
}


// Token Decoder
// to decode the token from the user details
module.exports.decode = token => {
    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);
    }

    return jwt.verify(token, secretKey, (error,data) => {
        if(error){
            return(error);
        }
        else{
            return(jwt.decode(token, {complete: true}).payload);
        }
    })
}

